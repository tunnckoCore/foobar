const lingGlob = '**/*.{js,ts,tsx,es6,jsx,mjs}';
export const lint = `eslint "${lingGlob}" --fix --quiet --format codeframe`;

const prettierGlob = '**/*.{md,mdx,json,yaml,yml,toml}';
export const prettify = `prettier "${prettierGlob}" --write`;

export const test = 'asia --require esm --reporter codeframe';

export const cov = 'nyc --require esm asia';

export const precommit = [
  'git add -A',
  'lint-staged',
  cov,
  'git status --porcelain',
];

export const commit = 'gitcommit -sS';
export const dry = ['git add -A', commit];

export const recinc = 'conventional-recommended-bump -p angular';

export const release = 'new-release';
