# FROM mhart/alpine-node:10
FROM node:10

ARG NPM_REGISTRY
ARG NPM_TOKEN
ARG GITHUB_TOKEN
ARG NOW_USER
ARG NOW_REPO
ARG NOW_CI
ARG CI

RUN rm -rf "${NOW_USER}/${NOW_REPO}"
RUN git clone "https://github.com/${NOW_USER}/${NOW_REPO}" "${NOW_USER}/${NOW_REPO}"
WORKDIR "/usr/src/${NOW_USER}/${NOW_REPO}"
COPY . "/usr/src/${NOW_USER}/${NOW_REPO}"

# Installation
RUN yarn install --frozen-lockfile --prefer-offline --silent \
  || yarn install --frozen-lockfile --silent

# Running tests
RUN yarn scripts test

# Settuping for auto release/publish flow

RUN echo "//${NPM_REGISTRY}/:_authToken=${NPM_TOKEN}" > .npmrc

RUN ls -al
RUN yarn scripts recinc
